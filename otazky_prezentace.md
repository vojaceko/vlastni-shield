# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** |20 hodin |
| jak se mi to podařilo rozplánovat |řekl bych že docela dobře možná bych přístě otestoval zda všechny součástky fungují |
| design zapojení | *https://gitlab.spseplzen.cz/vojaceko/vlastni-shield/-/blob/main/dokumentace/schema/el-schema.png* |
| proč jsem zvolil tento design | protože je minimalistický |
| zapojení | *https://gitlab.spseplzen.cz/vojaceko/vlastni-shield/-/blob/main/dokumentace/fotky/zapojeni.jpg* |
| z jakých součástí se zapojení skládá |kabely, rezistory, drátky |
| realizace | *https://gitlab.spseplzen.cz/vojaceko/vlastni-shield/-/blob/main/dokumentace/fotky/fin%C3%A1ln%C3%AD_f%C3%A1ze_shieldu.jpg* |
| nápad, v jakém produktu vše propojit dohromady|  |
| co se mi povedlo | zachování přehlednosti v zapojení díky rozlišné barvě kabelů |
| co se mi nepovedlo/příště bych udělal/a jinak |otestování jednotlivých součástek, správné zapojení fotorezistoru |
| zhodnocení celé tvorby | I přesto že jsem si myslel že to nebude nic co by mě mohlo bavit tak nakonec mi nevadilo u toho trávit čas, jelikož tvoříte něco svého a tak mi to docela i nakonec chytlo a začalo bavit (až na dokumentaci :D) |
| odkaz na video | https://www.youtube.com/watch?v=Ig7bRMSQqA8 | 