#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);

byte degreeSymbol[8] = {
  B00111,
  B00101,
  B00111,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000
};

void setupDisplay() {
  lcd.init();
  lcd.backlight();
  
  lcd.setCursor(0, 0);
  lcd.print("Teplota:");
  lcd.setCursor(0, 1);
  lcd.print("Jas:");

  lcd.createChar(0, degreeSymbol);
}

void vypisTeplota(float Temperature){
  lcd.setCursor(9, 0);
  lcd.print(Temperature);
  lcd.write(byte(0));
  lcd.print("C");

}
void vypisJas(int jas){
  int Tjas = map(jas, 0, 1024, 0, 100);
  lcd.setCursor(9, 1);
  lcd.print(Tjas);
  lcd.print("   ");
  delay(100);
}