#include <Adafruit_NeoPixel.h>

const int PIN = D3;
const int NUMPIXELS = 8;

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

void setupNeoPixel(){
  pixels.begin();
}

void NeoPixel(int jas){
  // Nastavení jasu LED na základě hodnoty 'jas'
  pixels.setBrightness(map(jas, 0, 1024, 5, 100));
  
  // Přemapování jasu na hodnoty pro červenou a modrou barvu
  int red = map(jas, 0, 1024, 255, 0);  // Čím vyšší jas, tím méně červené
  int blue = map(jas, 0, 1024, 0, 255); // Čím vyšší jas, tím více modré

  for (int i = 0; i < NUMPIXELS; i++) {
    // Nastavení barvy podle hodnot 'red' a 'blue', zelená složka zůstává 0
    pixels.setPixelColor(i, pixels.Color(red, 0, blue));  
  }
  
  // Aktualizace LED pásku
  pixels.show();
}
