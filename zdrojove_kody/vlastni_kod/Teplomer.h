#include <OneWire.h>              // Pro komunikaci s DS18B20
#include <DallasTemperature.h>    // Pro čtení dat z DS18B20

const int TEMP = D6;
OneWire oneWire(TEMP);
DallasTemperature sensors(&oneWire);

float Temp(){
  sensors.requestTemperatures();
  return sensors.getTempCByIndex(0);
}