const int RED = D7;
const int GREEN = D5;

void setupLed() {
  // Nastavení pinů jako výstupy
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
}

void Led(int teplo){

  if (teplo > 27){
    digitalWrite(RED, HIGH);
    digitalWrite(GREEN, LOW);
  } else if (teplo < 27){
    digitalWrite(GREEN, HIGH);
    digitalWrite(RED, LOW);
  }

}
