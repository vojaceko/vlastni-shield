#include "led.h"
#include "Display.h"
#include "Teplomer.h"
#include "NeoPixel.h"
#include "Fotorezistor.h"

void setup() {
  setupNeoPixel();
  setupLed();
  setupDisplay();
  setupFotorezistor();
}

void loop() {
  vypisJas(Jas());
  NeoPixel(Jas());
  vypisTeplota(Temp());
  Led(Temp());
}
